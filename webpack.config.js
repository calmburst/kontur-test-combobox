const configFileName = {
  development: `webpack.dev.config`,
  production: `webpack.prod.config`,
};

module.exports = require(`./webpack/${configFileName[process.env.NODE_ENV]}`);
