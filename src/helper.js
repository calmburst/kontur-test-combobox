export const clickedOutsideElement = (element, event) => {
  let eventTarget = event.target ? event.target : event.srcElement;

  while (eventTarget !== null) {
    if (eventTarget === element || !eventTarget) {
      return false;
    }
    eventTarget = eventTarget.offsetParent;
  }

  return true;
};
