/* eslint-disable */
export const KEY_CODES = {
  BACKSPACE: 8,
  CAPSLOCK : 20,
  DOWN     : 40,
  ENTER    : 13,
  ESCAPE   : 27,
  TAB      : 9,
  UP       : 38,
};
