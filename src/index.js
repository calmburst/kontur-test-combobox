import createHistory from 'history/createBrowserHistory';
import React from 'react';
import dva from 'dva';
import {
  Route,
  Router,
  Switch,
} from 'dva/router';

import Layout from 'pages/Layout';

import 'normalize.css';
import './base.scss';
import '../favicons';

const app = dva({ history: createHistory() });

app.router(({ history }) => (
  <Router history={history}>
    <Switch>
      <Route path='/' component={Layout} />
    </Switch>
  </Router>
));

app.start(`#app`);
