import React from 'react';
import PropTypes from 'prop-types';
import b_ from 'b_';
import cn from 'classnames';
import { NavLink } from 'dva/router';

import Logo from 'components/Logo';

import './style.scss';

const b = b_.lock(`Header`);

const Header = ({ className }) => (
  <header className={cn(b(), className)}>
    <Logo className={b(`logo`)} />
    <div className={b(`links`)}>
      <NavLink
        activeClassName={b(`links-item`, { active: true })}
        className={b(`links-item`)}
        to='/input'
      >
        Input
      </NavLink>
      <NavLink
        activeClassName={b(`links-item`, { active: true })}
        className={b(`links-item`)}
        to='/combobox'
      >
        Combobox
      </NavLink>
    </div>
  </header>
);

Header.propTypes = {
  className: PropTypes.string,
};

Header.defaultProps = {
  className: ``,
};

export default Header;
