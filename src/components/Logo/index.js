import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'dva/router';
import cn from 'classnames';
import b_ from 'b_';

import logoImg from './img/logo.png';
import './style.scss';

const b = b_.lock(`Logo`);

const Logo = ({ href, className }) => (
  <Link to={href} className={cn(b(), className)} >
    <img alt='logo' src={logoImg} className={b(`img`)} />
  </Link>
);

Logo.propTypes = {
  className: PropTypes.string,
  href     : PropTypes.string,
};

Logo.defaultProps = {
  className: ``,
  href     : `/`,
};

export default Logo;
