export default {
  buttons: {
    update: () => `Обновить`,
  },
  errors: {
    emptyValue: () => `Выберите значение из списка`,
    notFound  : () => `Не найдено`,
    server    : () => `Что-то пошло не так. Проверьте соединение с интернетом и поробуйте еще раз`,
  },
  labels: {
    loading: () => `Загрузка`,
  },
  messages: {
    shownCompletions: (shown, total) => (
      `Показано ${shown} из ${total} элементов.\nУточните запрос, чтобы увидеть остальные`
    ),
  },
};
