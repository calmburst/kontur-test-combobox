import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import _ from 'lodash';
import b_ from 'b_';
import cn from 'classnames';
import getDistance from 'js-levenshtein';

import { Scrollbars } from 'react-custom-scrollbars';
import Highlighter from 'react-highlight-words';

import { clickedOutsideElement } from 'helper';

import CircleProgress from 'components/CircleProgress';
import Input from 'components/Input';

import './style.scss';
import m from './message';

const b = b_.lock(`Combobox`);

class Combobox extends React.Component {
  constructor(props) {
    super(props);

    const {
      focused,
      items,
      onFetchItems,
      valid,
      value,
      warning,
    } = props;

    this.state = {
      completions: items,
      focused,
      inited     : false,
      page       : 1,
      valid,
      value,
      warning,
    };

    this.container = React.createRef();
    this.control = React.createRef();
    this.input = React.createRef();
    this.list = React.createRef();

    this.fetchItems = _.throttle(onFetchItems, 400);
    this.setCompletionsThrottled = _.throttle(this.setCompletions, 300);
  }

  componentDidMount() {
    if (this.state.focused) this.control.current.focus();
  }

  componentWillReceiveProps({ disabled }) {
    if (disabled) {
      this.setState({
        focused    : false,
        focusedItem: null,
      });
    }
    this.setCompletionsThrottled();
  }

  componentWillUnmount() {
    this.removeDocumentEventListeners();
  }

  onDocumentEvent = e => {
    if (!this.state.isOpen) return;

    if (e.type === `focus` && clickedOutsideElement(ReactDOM.findDOMNode(this.container.current), e)) {
      this.onBlur();
    } else {
      const eventOccuredOutsideList = clickedOutsideElement(ReactDOM.findDOMNode(this.list.current), e);
      const eventOccuredOutsideInput = clickedOutsideElement(ReactDOM.findDOMNode(this.input.current), e);
      const eventOccuredOutsideError = !_.includes(_.get(e, `target.className`), b(`list-error`));

      if (eventOccuredOutsideList && eventOccuredOutsideInput && eventOccuredOutsideError) {
        this.onBlur();
      }
    }
  };

  addDocumentEventListeners = () => {
    document.addEventListener(`click`, this.onDocumentEvent);
    document.addEventListener(`focus`, this.onDocumentEvent, true);
  };

  removeDocumentEventListeners() {
    document.removeEventListener(`click`, this.onDocumentEvent);
    document.removeEventListener(`focus`, this.onDocumentEvent, true);
  }

  setCompletions = () => this.setState(({ inputValue }, {
    items,
    keyLabel,
    maxCompletions,
    withArrow,
  }) => {
    let completions = [];
    let focusedItem = null;

    if (!inputValue) {
      completions = items.slice();
    } else {
      const lowerInputValue = _.toLower(inputValue);
      _.each(items, item => {
        const searchString = (!_.isEmpty(keyLabel) && _.isPlainObject(item))
          ? _.toLower(_.get(item, keyLabel, ``))
          : _.toLower(item);

        if (_.includes(searchString, lowerInputValue)) {
          completions.push({
            ...item,
            __distance: getDistance(searchString, lowerInputValue),
          });
        }
      });
      if (!_.isEmpty(completions)) {
        completions = _.sortBy(completions, `__distance`);
        focusedItem = 0;
      }
    }

    let listBottomNotification = null;
    if (!withArrow && completions.length > maxCompletions) {
      listBottomNotification = m.messages.shownCompletions(maxCompletions, completions.length);
      completions = _.take(completions, maxCompletions);
    }

    return ({
      completions,
      focusedItem,
      listBottomNotification,
      listTopNotification: _.isEmpty(completions) ? m.errors.notFound() : null,
      page               : 1,
    });
  })

  onInputChange = (inputValue = ``) => this.setState(
    {
      inputValue,
      isOpen: !!inputValue.length,
      value : null,
    }, () => {
      if (this.props.fetchOnInputChange) this.fetchItems(inputValue);
      this.setCompletionsThrottled();
      this.props.onInputChange(inputValue);
    }
  );

  onSelect = value => {
    const {
      keyLabel,
      onChange,
    } = this.props;

    const inputValue = (!_.isEmpty(keyLabel) && _.isPlainObject(value)) ? _.get(value, keyLabel) : value;

    this.setState({
      focused     : false,
      focusedItem : null,
      inputValue,
      isOpen      : false,
      notification: null,
      valid       : true,
      value,
    }, () => {
      this.container.current.nextSibling.focus();
      this.setCompletions();
      onChange(_.omit(value, `__distance`));
    });
  }

  onFocus = () => this.setState((
    {
      inited,
      inputValue,
      isOpen,
      value,
    }, {
      fetchOnInputChange,
      withArrow,
    }
  ) => {
    this.addDocumentEventListeners();

    if (!_.isEmpty(value)) this.input.current.control.current.setSelectionRange(0, inputValue.length);
    if ((withArrow || !fetchOnInputChange) && !inited) this.fetchItems(inputValue);

    return {
      focused     : true,
      inited      : true,
      isOpen      : withArrow || isOpen,
      notification: null,
      valid       : true,
      warning     : false,
    };
  });

  onBlur = () => {
    const {
      completions,
      inputValue,
      value,
    } = this.state;

    if (_.isEmpty(value) && !_.get(completions, `[0].__distance`, 1)) {
      this.onSelect(completions[0]);
    } else {
      const valid = !_.isEmpty(value) || _.isEmpty(inputValue);
      this.setState({
        focused     : false,
        isOpen      : false,
        notification: valid ? null : m.errors.emptyValue(),
        valid,
      });
    }
  }

  onFocusListItem = focusedItem => this.setState({ focusedItem });

  onBlurListItem = () => this.setState({ focusedItem: null });

  onListScroll = ({ scrollHeight, scrollTop }) => {
    const { page } = this.state;

    if (scrollHeight - scrollTop < 600) {
      this.setState({ page: page + 1 });
    } else if (scrollHeight - scrollTop > 2500 && page > 1) {
      this.setState({ page: page - 1 });
    }
  }

  onDownPress = () => {
    this.setState(({ isOpen, completions, focusedItem }) => {
      const shouldFocusList = completions.length > 1 && isOpen;
      let nextFocusedListItem = focusedItem;

      if (_.isNil(focusedItem)) {
        nextFocusedListItem = 0;
      } else if (nextFocusedListItem < completions.length - 1) {
        nextFocusedListItem += 1;
      }

      return {
        focusedItem: shouldFocusList ? nextFocusedListItem : null,
      };
    });
  }

  onUpPress = () => {
    this.setState(({ isOpen, completions, focusedItem }) => {
      const shouldFocusList = completions.length > 1 && isOpen && !_.isNil(focusedItem);
      let nextFocusedListItem = _.isNil(focusedItem) ? 0 : focusedItem;

      if (nextFocusedListItem > 0) nextFocusedListItem -= 1;

      return {
        focusedItem: shouldFocusList ? nextFocusedListItem : null,
      };
    });
  }

  onEnterPress = () => {
    const {
      completions,
      focusedItem,
      isOpen,
    } = this.state;

    if (isOpen && !_.isEmpty(completions) && !_.isNil(focusedItem)) {
      this.onSelect(completions[focusedItem]);
    }
  }

  onEscapePress = () => this.setState({ isOpen: false });

  renderError = () => (
    <div className={b(`list-error`)}>
      <div className={b(`list-error-message`)}>{m.errors.server()}</div>
      <button
        className={b(`list-error-button`)}
        onClick={this.props.onFetchItems}
      >
        {m.buttons.update()}
      </button>
    </div>
  )

  renderList = () => {
    const {
      completions,
      focusedItem,
      inputValue,
      isOpen,
      listBottomNotification,
      listTopNotification,
      page,
      value,
    } = this.state;

    const {
      isLoading,
      keyLabel,
      withError,
    } = this.props;

    if (!isOpen) return null;

    return (
      <div className={b(`list`)} ref={this.list}>
        <Scrollbars
          autoHeight
          autoHeightMax={450}
          autoHeightMin={0}
          hideTracksWhenNotNeeded
          onScrollFrame={this.onListScroll}
        >
          {isLoading ?
            <CircleProgress className={b(`list-loader`)} label={m.labels.loading()} /> :
            <React.Fragment>
              {withError ? this.renderError() :
                (
                  <React.Fragment>
                    {listTopNotification &&
                      <div className={b(`list-notification`, { top: true })}>{listTopNotification}</div>
                    }
                    {!_.isEmpty(completions) && _.map(_.take(completions, page * 50), (item, key) => (
                      <div
                        className={b(`list-item`, {
                          focused : focusedItem === key,
                          selected: _.isEqual(item, value),
                        })}
                        key={key}
                        onBlur={this.onBlurListItem}
                        onClick={() => this.onSelect(item)}
                        onFocus={() => this.onFocusListItem(key)}
                        onMouseEnter={() => this.onFocusListItem(key)}
                        onMouseLeave={this.onBlurListItem}
                      >
                        <Highlighter
                          autoEscape
                          className={b(`list-item-content`)}
                          highlightClassName={b(`list-item-content`, { highlight: true })}
                          searchWords={[inputValue]}
                          textToHighlight={!_.isEmpty(keyLabel) && _.isPlainObject(item) ? _.get(item, keyLabel) : item}
                        />
                      </div>
                    ))}
                    {listBottomNotification &&
                      <div className={b(`list-notification`, { bottom: true })}>{listBottomNotification}</div>
                    }
                  </React.Fragment>
                )
              }
            </React.Fragment>
          }
        </Scrollbars>
      </div>
    );
  }

  renderNotification = () => {
    const {
      isOpen,
      notification,
      valid,
      warning,
    } = this.state;

    if (isOpen || !notification || (valid !== false && warning !== true)) return null;

    return (
      <div
        className={cn(
          b(`notification`, {
            invalid: valid === false,
            warning: warning === true,
          }),
          this.props.notificationClassName
        )}
      >
        {notification}
      </div>
    );
  }

  render() {
    const {
      className,
      disabled,
      inputClassName,
      label,
      labelClassName,
      labelPosition,
      name,
      placeholder,
      size,
      withArrow,
    } = this.props;
    const {
      focused,
      inputValue,
      valid,
      warning,
    } = this.state;

    return (
      <div
        className={cn(
          b({
            invalid: valid === false,
            labelPosition,
            size,
          }),
          className
        )}
        ref={this.container}
      >
        {label && <span className={cn(b(`label`), labelClassName)}>{label}</span>}
        <div className={b(`input-wrapper`)}>
          <Input
            className={cn(b(`input`), inputClassName)}
            disabled={disabled}
            focused={focused}
            iconRight={withArrow ? `arrowDown` : null}
            labelClassName={labelClassName}
            name={name}
            onChange={this.onInputChange}
            onDownPress={this.onDownPress}
            onEnterPress={this.onEnterPress}
            onEscapePress={this.onEscapePress}
            onFocus={this.onFocus}
            onUpPress={this.onUpPress}
            placeholder={placeholder}
            ref={this.input}
            size={size}
            valid={valid}
            value={inputValue}
            warning={warning}
          />
          {this.renderList()}
          {this.renderNotification()}
        </div>
      </div>
    );
  }
}

Combobox.propTypes = {
  className         : PropTypes.string,
  disabled          : PropTypes.bool,
  fetchOnInputChange: PropTypes.bool,
  focused           : PropTypes.bool,
  label             : PropTypes.string,
  labelClassName    : PropTypes.string,
  labelPosition     : PropTypes.oneOf([
    `left`,
    `top`,
  ]),
  maxCompletions       : PropTypes.number,
  name                 : PropTypes.string,
  notificationClassName: PropTypes.string,
  onChange             : PropTypes.func,
  onFetchItems         : PropTypes.func,
  onInputChange        : PropTypes.func,
  placeholder          : PropTypes.string,
  size                 : PropTypes.oneOf([
    `large`,
    `medium`,
    `small`,
  ]),
  valid: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.bool,
  ]),
  value: PropTypes.oneOfType([
    PropTypes.shape({}),
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool,
  ]),
  warning: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.bool,
  ]),
};

Combobox.defaultProps = {
  className            : ``,
  disabled             : false,
  fetchOnInputChange   : true,
  focused              : false,
  label                : ``,
  labelClassName       : ``,
  labelPosition        : `top`,
  maxCompletions       : 5,
  name                 : ``,
  notificationClassName: ``,
  onChange             : _.noop,
  onFetchItems         : _.noop,
  onInputChange        : _.noop,
  placeholder          : ``,
  size                 : `medium`,
  valid                : null,
  value                : null,
  warning              : null,
};

export default Combobox;
