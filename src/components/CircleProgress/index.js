import React from 'react';
import PropTypes from 'prop-types';
import b_ from 'b_';
import cn from 'classnames';

import './style.scss';

const b = b_.lock(`CircleProgress`);

const CircleProgress = ({
  className,
  label,
}) => (
  <div className={cn(b(), className)}>
    <div className={b(`loader`)}><div className={b(`loader-spinner`)} /></div>
    {label && <div className={b(`label`)}>{label}</div>}
  </div>
);

CircleProgress.propTypes = {
  className: PropTypes.string,
  label    : PropTypes.string,
};

CircleProgress.defaultProps = {
  className: ``,
  label    : ``,
};

export default CircleProgress;
