import React from 'react';
import PropTypes from 'prop-types';
import b_ from 'b_';
import cn from 'classnames';

import Icon from 'components/Icon';

import { KEY_CODES } from '../../constants';
import './style.scss';

const b = b_.lock(`Input`);

class Input extends React.Component {
  constructor(props) {
    super(props);

    const {
      focused,
      valid,
      value,
      warning,
    } = props;

    this.state = {
      focused,
      valid,
      value,
      warning,
    };

    this.control = React.createRef();
  }

  componentDidMount() {
    if (this.state.focused) this.control.current.focus();
  }

  componentWillReceiveProps({
    focused,
    valid,
    value,
    warning,
  }) {
    if (warning !== this.state.warning || valid !== this.state.valid) this.setState({ valid, warning });
    if (focused !== this.state.focused) this.setState({ focused });
    if (value !== this.state.value) this.setState({ value });
  }

  onFocus = event => {
    this.setState({
      focused: true,
      valid  : null,
      warning: null,
    });

    this.control.current.focus();

    this.props.onFocus(event);
  };

  onBlur = event => {
    this.setState({
      focused: false,
      valid  : this.props.valid,
      warning: this.props.warning,
    });

    this.props.onBlur(event);
  };

  onChange(value) {
    this.setState({ value });
    this.props.onChange(value);
  }

  onKeyDown = event => {
    const {
      onDownPress,
      onEnterPress,
      onEscapePress,
      onUpPress,
    } = this.props;

    switch (event.keyCode) {
      case KEY_CODES.ENTER:
        onEnterPress(event);
        break;
      case KEY_CODES.ESCAPE:
        onEscapePress(event);
        break;
      case KEY_CODES.DOWN:
        onDownPress(event);
        break;
      case KEY_CODES.UP:
        onUpPress(event);
        break;
    }
  };

  render() {
    const {
      disabled,
      iconLeft,
      iconRight,
      label,
      labelClassName,
      labelPosition,
      name,
      notification,
      notificationClassName,
      placeholder,
      size,
      type,
    } = this.props;
    const {
      focused,
      valid,
      value,
      warning,
    } = this.state;

    const className = cn(b({
      disabled,
      filled        : !!value,
      focused,
      [`icon-left`] : !!iconLeft,
      [`icon-right`]: !!iconRight,
      invalid       : valid === false,
      labelPosition,
      size,
      warning,
    }), this.props.className);

    return (
      <span className={className}>
        <label className={b(`box`)}>
          {label && <span className={cn(b(`label`), labelClassName)}>{label}</span>}
          <span className={b(`control-wrapper`)}>
            {iconLeft && <Icon className={b(`icon`, { left: true })} icon={iconLeft} />}
            <input
              autoComplete='off'
              className={b(`control`)}
              disabled={disabled}
              name={name}
              onBlur={this.onBlur}
              onChange={event => this.onChange(event.target.value)}
              onFocus={this.onFocus}
              onKeyDown={this.onKeyDown}
              placeholder={placeholder}
              ref={this.control}
              type={type}
              value={value}
            />
            {iconRight && <Icon className={b(`icon`, { right: true })} icon={iconRight} />}
            {(valid === false || warning === true) && notification &&
              <span className={cn(b(`notification`), notificationClassName)}>{notification}</span>
            }
          </span>
        </label>
      </span>
    );
  }
}

Input.propTypes = {
  className     : PropTypes.string,
  disabled      : PropTypes.bool,
  focused       : PropTypes.bool,
  iconLeft      : PropTypes.string,
  iconRight     : PropTypes.string,
  label         : PropTypes.string,
  labelClassName: PropTypes.string,
  labelPosition : PropTypes.oneOf([
    `left`,
    `top`,
  ]),
  name                 : PropTypes.string,
  notification         : PropTypes.string,
  notificationClassName: PropTypes.string,
  onBlur               : PropTypes.func,
  onChange             : PropTypes.func,
  onDownPress          : PropTypes.func,
  onEnterPress         : PropTypes.func,
  onEscapePress        : PropTypes.func,
  onFocus              : PropTypes.func,
  onUpPress            : PropTypes.func,
  placeholder          : PropTypes.string,
  size                 : PropTypes.oneOf([
    `large`,
    `medium`,
    `small`,
  ]),
  type : PropTypes.string,
  valid: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.bool,
  ]),
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  warning: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.bool,
  ]),
};

Input.defaultProps = {
  className            : ``,
  disabled             : false,
  focused              : false,
  iconLeft             : ``,
  iconRight            : ``,
  label                : ``,
  labelClassName       : ``,
  labelPosition        : `top`,
  name                 : ``,
  notification         : ``,
  notificationClassName: ``,
  onBlur               : () => {},
  onChange             : () => {},
  onDownPress          : () => {},
  onEnterPress         : () => {},
  onEscapePress        : () => {},
  onFocus              : () => {},
  onUpPress            : () => {},
  placeholder          : ``,
  size                 : `medium`,
  type                 : `text`,
  valid                : null,
  value                : ``,
  warning              : null,
};

export default Input;
