import React from 'react';
import PropTypes from 'prop-types';
import b_ from 'b_';
import cn from 'classnames';

import './style.scss';

const b = b_.lock(`Icon`);

const ICONS = {
  arrowDown: `&#xe800;`,
  search   : `&#xe801;`,
};

const Icon = ({
  className,
  icon,
}) => (
  <span
    className={cn(b({ [icon]: true }), className)}
    dangerouslySetInnerHTML={{ __html: ICONS[icon] ? ICONS[icon] : `` }}
  />
);

Icon.propTypes = {
  className: PropTypes.string,
  icon     : PropTypes.oneOf([
    `arrowDown`,
    `search`,
  ]).isRequired,
};

Icon.defaultProps = {
  className: ``,
};

export default Icon;
