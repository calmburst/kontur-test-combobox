import React from 'react';
import b_ from 'b_';
import _ from 'lodash';

import {
  RadioGroup,
  Radio,
} from 'react-radio-group';

import Input from 'components/Input';
import Combobox from 'components/Combobox';

import mock from './mock';
import './style.scss';

const b = b_.lock(`PageCombobox`);

const getCities = (timeout, failPossibility) => new Promise((resolve, reject) => {
  const shouldCrash = _.random(0, 100) < failPossibility;
  setTimeout(() => {
    if (shouldCrash) {
      reject();
    } else {
      resolve(mock);
    }
  }, timeout);
});

class PageCombobox extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      disabled          : false,
      failPossibility   : 30,
      fetchOnInputChange: true,
      isLoading         : false,
      items             : [],
      keyLabel          : `City`,
      label             : `Город`,
      labelPosition     : `top`,
      maxCompletions    : 5,
      placeholder       : `Начните вводить код или название`,
      size              : `medium`,
      timeout           : 300,
      withArrow         : true,
      withError         : false,
    };
  }

  onChange = field => this.setState(field);

  getItems = async () => {
    if (this.state.isLoading) return;
    this.setState(() => ({
      isLoading: true,
      withError: false,
    }));

    let items = [];

    try {
      items = await getCities(this.state.timeout, this.state.failPossibility);
      this.setState({
        isLoading: false,
        items,
        withError: false,
      });
    } catch (e) {
      this.setState({
        isLoading: false,
        items,
        withError: true,
      });
    }
  }

  render() {
    return (
      <div className={b()}>
        <Combobox
          className={b(`combobox`)}
          inputClassName={b(`combobox-input`)}
          onFetchItems={this.getItems}
          {..._.omit(this.state, `timeout`)}
        />

        <section className={b(`controls`)}>
          <h3 className={b(`controls-title`)}>settings</h3>
          <Input
            className={b(`controls-input`)}
            label='Таймаут ответа сервера, ms'
            labelClassName={b(`controls-label`)}
            onChange={timeout => this.onChange({ timeout: _.toNumber(timeout) })}
            value={this.state.timeout}
          />
          <Input
            className={b(`controls-input`)}
            label='Вероятность ошибки сервера, %'
            labelClassName={b(`controls-label`)}
            onChange={failPossibility => this.onChange({ failPossibility: _.toNumber(failPossibility) })}
            value={this.state.failPossibility}
          />
          <h3 className={b(`controls-title`)}>props</h3>
          <span className={b(`controls-label`)} >disabled</span>
          <RadioGroup
            className={b(`controls-radio-group`)}
            name='disabled'
            onChange={disabled => this.onChange({ disabled })}
            selectedValue={this.state.disabled}
          >
            <Radio className={b(`controls-radio-group-item`)} value />true
            <Radio className={b(`controls-radio-group-item`)} value={false} />false
          </RadioGroup>

          <span className={b(`controls-label`)} >
            fetchOnInputChange (посылать ли запрос на сервер при изменении вводимого значения)
          </span>
          <RadioGroup
            className={b(`controls-radio-group`)}
            name='fetchOnInputChange'
            onChange={fetchOnInputChange => this.onChange({ fetchOnInputChange })}
            selectedValue={this.state.fetchOnInputChange}
          >
            <Radio className={b(`controls-radio-group-item`)} value />true
            <Radio className={b(`controls-radio-group-item`)} value={false} />false
          </RadioGroup>

          <Input
            className={b(`controls-input`)}
            label='label'
            labelClassName={b(`controls-label`)}
            onChange={label => this.onChange({ label })}
            value={this.state.label}
          />

          <span className={b(`controls-label`)} >labelPosition</span>
          <RadioGroup
            className={b(`controls-radio-group`)}
            name='labelPosition'
            onChange={labelPosition => this.onChange({ labelPosition })}
            selectedValue={this.state.labelPosition}
          >
            <Radio className={b(`controls-radio-group-item`)} value='top' />top
            <Radio className={b(`controls-radio-group-item`)} value='left' />left
          </RadioGroup>

          <Input
            className={b(`controls-input`)}
            label='maxCompletions (when withArrow set to false)'
            labelClassName={b(`controls-label`)}
            onChange={maxCompletions => this.onChange({ maxCompletions: _.toSafeInteger(maxCompletions) })}
            value={this.state.maxCompletions}
          />

          <Input
            className={b(`controls-input`)}
            label='placeholder'
            labelClassName={b(`controls-label`)}
            onChange={placeholder => this.onChange({ placeholder })}
            value={this.state.placeholder}
          />

          <span className={b(`controls-label`)} >size</span>
          <RadioGroup
            className={b(`controls-radio-group`)}
            name='size'
            onChange={size => this.onChange({ size })}
            selectedValue={this.state.size}
          >
            <Radio className={b(`controls-radio-group-item`)} value='small' />small
            <Radio className={b(`controls-radio-group-item`)} value='medium' />medium
            <Radio className={b(`controls-radio-group-item`)} value='large' />large
          </RadioGroup>

          <span className={b(`controls-label`)}>withArrow</span>
          <RadioGroup
            className={b(`controls-radio-group`)}
            name='withArrow'
            onChange={withArrow => this.onChange({ withArrow })}
            selectedValue={this.state.withArrow}
          >
            <Radio className={b(`controls-radio-group-item`)} value />true
            <Radio className={b(`controls-radio-group-item`)} value={false} />false
          </RadioGroup>
        </section>
      </div>
    );
  }
}

export default PageCombobox;
