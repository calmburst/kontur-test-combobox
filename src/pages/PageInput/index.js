import React from 'react';
import b_ from 'b_';

import {
  RadioGroup,
  Radio,
} from 'react-radio-group';

import Input from 'components/Input';

import './style.scss';

const b = b_.lock(`PageInput`);

class PageInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      disabled     : false,
      iconLeft     : ``,
      iconRight    : ``,
      label        : `Город`,
      labelPosition: `top`,
      notification : `Некорректное значение`,
      placeholder  : `Начните вводить код или название`,
      size         : `medium`,
      valid        : true,
      value        : ``,
      warning      : null,
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(field) {
    this.setState(field);
  }

  render() {
    return (
      <div className={b()}>
        <Input
          className={b(`input`)}
          onChange={value => this.onChange({ value })}
          {...this.state}
        />

        <section className={b(`controls`)}>
          <h3 className={b(`controls-title`)}>props</h3>
          <span className={b(`controls-label`)} >disabled</span>
          <RadioGroup
            className={b(`controls-radio-group`)}
            name='disabled'
            onChange={disabled => this.onChange({ disabled })}
            selectedValue={this.state.disabled}
          >
            <Radio className={b(`controls-radio-group-item`)} value />true
            <Radio className={b(`controls-radio-group-item`)} value={false} />false
          </RadioGroup>

          <div className={b(`controls-row`)}>
            <div className={b(`controls-column`)}>
              <span className={b(`controls-label`)} >iconLeft</span>
              <RadioGroup
                className={b(`controls-radio-group`)}
                name='iconLeft'
                onChange={iconLeft => this.onChange({ iconLeft })}
                selectedValue={this.state.iconLeft}
              >
                <Radio className={b(`controls-radio-group-item`)} value='' />none
                <Radio className={b(`controls-radio-group-item`)} value='arrowDown' />arrowDown
                <Radio className={b(`controls-radio-group-item`)} value='search' />search
              </RadioGroup>
            </div>

            <div className={b(`controls-column`)}>
              <span className={b(`controls-label`)} >iconRight</span>
              <RadioGroup
                className={b(`controls-radio-group`)}
                name='iconRight'
                onChange={iconRight => this.onChange({ iconRight })}
                selectedValue={this.state.iconRight}
              >
                <Radio className={b(`controls-radio-group-item`)} value='' />none
                <Radio className={b(`controls-radio-group-item`)} value='arrowDown' />arrowDown
                <Radio className={b(`controls-radio-group-item`)} value='search' />search
              </RadioGroup>
            </div>
          </div>

          <Input
            className={b(`controls-input`)}
            label='label'
            labelClassName={b(`controls-label`)}
            onChange={label => this.onChange({ label })}
            value={this.state.label}
          />

          <span className={b(`controls-label`)} >labelPosition</span>
          <RadioGroup
            className={b(`controls-radio-group`)}
            name='labelPosition'
            onChange={labelPosition => this.onChange({ labelPosition })}
            selectedValue={this.state.labelPosition}
          >
            <Radio className={b(`controls-radio-group-item`)} value='top' />top
            <Radio className={b(`controls-radio-group-item`)} value='left' />left
          </RadioGroup>

          <Input
            className={b(`controls-input`)}
            label='notification'
            labelClassName={b(`controls-label`)}
            onChange={notification => this.onChange({ notification })}
            value={this.state.notification}
          />

          <Input
            className={b(`controls-input`)}
            label='placeholder'
            labelClassName={b(`controls-label`)}
            onChange={placeholder => this.onChange({ placeholder })}
            value={this.state.placeholder}
          />

          <span className={b(`controls-label`)} >size</span>
          <RadioGroup
            className={b(`controls-radio-group`)}
            name='size'
            onChange={size => this.onChange({ size })}
            selectedValue={this.state.size}
          >
            <Radio className={b(`controls-radio-group-item`)} value='small' />small
            <Radio className={b(`controls-radio-group-item`)} value='medium' />medium
            <Radio className={b(`controls-radio-group-item`)} value='large' />large
          </RadioGroup>

          <div className={b(`controls-row`)}>
            <div className={b(`controls-column`)}>
              <span className={b(`controls-label`)}>valid</span>
              <RadioGroup
                className={b(`controls-radio-group`)}
                name='valid'
                onChange={valid => this.onChange({ valid })}
                selectedValue={this.state.valid}
              >
                <Radio className={b(`controls-radio-group-item`)} value />true
                <Radio className={b(`controls-radio-group-item`)} value={false} />false
              </RadioGroup>
            </div>

            <div className={b(`controls-column`)}>
              <span className={b(`controls-label`)}>warning</span>
              <RadioGroup
                className={b(`controls-radio-group`)}
                name='warning'
                onChange={warning => this.onChange({ warning })}
                selectedValue={this.state.warning}
              >
                <Radio className={b(`controls-radio-group-item`)} value />true
                <Radio className={b(`controls-radio-group-item`)} value={false} />false
              </RadioGroup>
            </div>
          </div>

          <Input
            className={b(`controls-input`)}
            label='value'
            labelClassName={b(`controls-label`)}
            onChange={value => this.onChange({ value })}
            value={this.state.value}
          />
        </section>
      </div>
    );
  }
}

export default PageInput;
