import React from 'react';
import b_ from 'b_';
import {
  Redirect,
  Route,
  Switch,
} from 'dva/router';

import Header from 'components/Header';
import PageInput from 'pages/PageInput';
import PageCombobox from 'pages/PageCombobox';

import './style.scss';

const b = b_.lock(`Layout`);

const Layout = () => (
  <div className={b()}>
    <Header className={b(`header`)} />
    <div className={b(`body`)}>
      <Switch>
        <Route path='/input' exact component={PageInput} />
        <Route path='/combobox' exact component={PageCombobox} />
        <Redirect from='/' to='/combobox' />
      </Switch>
    </div>
  </div>
);

export default Layout;
