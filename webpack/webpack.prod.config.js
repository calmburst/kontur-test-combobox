const ExtractTextPlugin = require(`extract-text-webpack-plugin`);
const UglifyJsPlugin = require(`uglifyjs-webpack-plugin`);

const extendBaseConfig = require(`./extendBaseConfig`);

module.exports = extendBaseConfig({
  output: {
    filename     : `index.[chunkhash:6].min.js`,
    chunkFilename: `[name].[chunkhash:6].min.js`,
  },

  module: {
    rules: [
      {
        test  : /\.(css|scss|sass)$/,
        loader: ExtractTextPlugin.extract({
          fallback: `style-loader`,
          use     : [
            {
              loader : `css-loader`,
              options: {
                minimize: true,
              },
            },
            {
              loader : `postcss-loader`,
              options: {
                ident  : `postcss`,
                plugins: () => [
                  require(`postcss-smart-import`),
                  require(`rucksack-css`),
                  require(`autoprefixer`)({
                    browsers: `last 2 versions`,
                    flexbox : `no-2009`,
                  }),
                ],
              },
            },
            {
              loader: `resolve-url-loader`,
            },
            {
              loader: `sass-loader`,
            },
          ],
        }),
      },
    ],
  },

  plugins: [
    new UglifyJsPlugin(),

    new ExtractTextPlugin(`[name].css`, { allChunks: true }),
  ],
});
