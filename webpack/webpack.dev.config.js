const webpack = require(`webpack`);

const extendBaseConfig = require(`./extendBaseConfig`);

module.exports = extendBaseConfig({
  watch: true,

  devtool: `eval-source-map`,

  entry: {
    main: [
      `webpack/hot/only-dev-server`,
      `webpack-dev-server/client?http://localhost:8000`,
    ],
  },

  devServer: {
    host              : `localhost`,
    port              : 8000,
    hot               : true,
    progress          : true,
    inline            : true,
    open              : true,
    historyApiFallback: true,
  },

  module: {
    rules: [
      {
        test: /\.(css|scss|sass)$/,
        use : [
          {
            loader: `style-loader`,
          },
          {
            loader : `css-loader`,
            options: {
              minimize: true,
            },
          },
          {
            loader : `postcss-loader`,
            options: {
              ident  : `postcss`,
              plugins: () => [
                require(`postcss-smart-import`),
                require(`rucksack-css`),
                require(`autoprefixer`)({
                  browsers: `last 2 versions`,
                  flexbox : `no-2009`,
                }),
              ],
            },
          },
          {
            loader: `resolve-url-loader`,
          },
          {
            loader: `sass-loader`,
          },
        ],
      },
    ],
  },

  plugins: [
    new webpack.NamedModulesPlugin(),

    new webpack.HotModuleReplacementPlugin(),
  ],
});
