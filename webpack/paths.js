const path = require(`path`);

const root = path.resolve(__dirname, `..`);
const resolve = relativePath => path.resolve(root, relativePath);

module.exports = {
  root,
  src     : resolve(`src`),
  build   : resolve(`public`),
  fonts   : resolve(`src/desgin/fonts`),
  html    : resolve(`webpack/template.html`),
  babelEnv: resolve(`node_modules/babel-preset-env`),
};
