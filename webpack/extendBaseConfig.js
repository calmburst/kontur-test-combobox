const CaseSensitivePathsPlugin = require(`case-sensitive-paths-webpack-plugin`);
const HtmlWebpackPlugin = require(`html-webpack-plugin`);
const LodashModuleReplacementPlugin = require(`lodash-webpack-plugin`);
const webpack = require(`webpack`);
const _ = require(`lodash`);

const paths = require(`./paths`);

module.exports = function extendBaseConfig(config) {
  return _.mergeWith({}, BASE_CONFIG, config, (objValue, srcValue) => {
    if (_.isArray(objValue)) {
      return objValue.concat(srcValue);
    }
  });
};

const BASE_CONFIG = {
  resolve: {
    extensions: [`.js`, `.jsx`, `.json`, `.css`, `.scss`, `.saas`],
    modules   : [`node_modules`, paths.src],
  },

  entry: {
    main: [
      `babel-polyfill`,
      paths.src,
    ],
  },

  output: {
    path         : paths.build,
    publicPath   : `/`,
    filename     : `[name].bundle.js`,
    chunkFilename: `[name].[chunkhash:6].js`,
  },

  plugins: [
    new LodashModuleReplacementPlugin({
      caching    : true,
      cloning    : true,
      coercions  : true,
      collections: true,
      flattening : true,
      memoizing  : true,
      paths      : true,
      shorthands : true,
    }),

    new CaseSensitivePathsPlugin(),

    new HtmlWebpackPlugin({
      filename: `index.html`,
      template: paths.html,
    }),

    new webpack.ContextReplacementPlugin(/\/moment\/locale\//, /ru|en-gb/),

    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    }),
  ],

  module: {
    rules: [{
      test   : /\.(js|jsx)$/,
      exclude: /node_modules/,
      use    : [{
        loader : `babel-loader`,
        options: {
          plugins: [`lodash`],
          presets: [
            [
              paths.babelEnv,
              {
                modules: false,
                targets: {
                  node: 6,
                },
              },
            ],
          ],
        },
      },
      { loader: `eslint-loader` },
      ],
    },

    {
      test: /\.jpe?g|\.gif|\.png|\.ico|\.svg/,
      use : [{
        loader : `url-loader`,
        options: {
          limit: 10000,
          name : `[name].[hash:6].[ext]`,
        },
      },
      { loader: `image-webpack-loader` },
      ],
    },
    {
      test: /\.(eot|svg|otf|ttf|woff|woff2)$/,
      use : [{
        loader : `file-loader`,
        options: {
          name: `[path][name].[ext]`,
        },
      }],
    },
    ],
  },
};
