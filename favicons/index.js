/* eslint-disable no-unused-vars */
const req = require.context(`!file-loader?name=favicons/[name].[ext]!./`, false, /\./);

let favicons = [
  `./favicon.ico`,
];

favicons = favicons.map(file => req(file));
/* eslint-enable no-unused-vars */
