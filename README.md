# kontur-test-combobox

* Создан компонент Input 
* Создан компонент Combobox (обязательные пункты)

## Установка и запуск  

```sh
$ npm i
$ npm start
```
Приложение доступно по адресу:
http://localhost:8000/

## Структура
| Страница | Адрес |
| ------ | ------ |
| Input | http://localhost:8000/input |
| Combobox | http://localhost:8000/combobox |

## Сборка
```sh
$ npm i
$ npm run build
```